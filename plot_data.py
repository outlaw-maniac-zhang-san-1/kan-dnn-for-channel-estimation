import numpy as np
import matplotlib.pyplot as plt
import os

# Load data
data_dir = '/code/dnn/OFDM_DNN-master/resnet/'
file_name = 'SNRdb_20_Num_Pilot_64'
train_losses = np.load(os.path.join(data_dir, f'train_losses_{file_name}.npy'))
train_error_rates = np.load(os.path.join(data_dir, f'train_error_rates_{file_name}.npy'))
test_error_rates = np.load(os.path.join(data_dir, f'test_error_rates_{file_name}.npy'))
epoch_times = np.load(os.path.join(data_dir, f'epoch_times_{file_name}.npy'))
data_load_times = np.load(os.path.join(data_dir, f'data_load_times_{file_name}.npy'))

def annotate_plot(x, y):
    if len(x) > 0 and len(y) > 0:
        indices_to_annotate = list(range(0, len(x), 20)) + [len(x) - 1]
        for i in indices_to_annotate:
            plt.annotate(f'{y[i]:.5f}', (x[i], y[i]), textcoords="offset points", xytext=(0, 5), ha='center')

def plot_with_mean(x, y, label, ylabel, title, filename):
    plt.figure()
    plt.plot(x, y, label=label)
    annotate_plot(x, y)
    mean_y = np.mean(y)
    plt.axhline(mean_y, color='r', linestyle='--', label=f'Mean: {mean_y:.2f}')
    plt.annotate(f'Mean: {mean_y:.2f}', xy=(0, mean_y), xytext=(5, -10), textcoords='offset points', ha='left', color='r')
    plt.xlabel('Epochs')
    plt.ylabel(ylabel)
    plt.legend()
    plt.title(title)
    plt.savefig(os.path.join(data_dir, filename))

# Plot training loss over epochs
plot_with_mean(range(len(train_losses)), train_losses, 'Training Loss', 'Loss', 'Training Loss Over Epochs', f'train_losses_{file_name}.png')

# Plot error rates over epochs
plt.figure()
plt.plot(train_error_rates, label='Training Error Rate')
annotate_plot(range(len(train_error_rates)), train_error_rates)
plt.plot(test_error_rates, label='Test Error Rate')
annotate_plot(range(len(test_error_rates)), test_error_rates)
plt.xlabel('Epochs')
plt.ylabel('Error Rate')
plt.legend()
plt.title('Error Rate Over Epochs')
plt.savefig(os.path.join(data_dir, f'error_rates_{file_name}.png'))

# Plot epoch times
plot_with_mean(range(len(epoch_times)), epoch_times, 'Epoch Time', 'Time (s)', 'Epoch Time Over Epochs', f'epoch_times_{file_name}.png')

# Plot data load times
plot_with_mean(range(len(data_load_times)), data_load_times, 'Data Load Time', 'Time (s)', 'Data Load Time Over Epochs', f'data_load_times_{file_name}.png')
